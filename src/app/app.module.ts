import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';


import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {AppRoutingModule} from './/app-routing.module';
import {BoardComponent} from './board/board.component';

import {FilterPipe} from './pipe/filter.pipe';
import {JsonfieldPipe} from './pipe/jsonfield.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BoardComponent,
    FilterPipe,
    JsonfieldPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
