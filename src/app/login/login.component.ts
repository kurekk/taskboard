import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {User} from '../model/user'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User("", "");

  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  loginUser(){
    this.user = new User("", "");
    this.router.navigate(["/board"])
  }

}
