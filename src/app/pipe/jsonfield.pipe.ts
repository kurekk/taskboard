import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonfield'
})

export class JsonfieldPipe implements PipeTransform{
  transform(items: any[], searchField: string, expectedValue: any): any[]{
    if (!items) return [];
    if (!searchField) return items;
    console.log("filtering json fileds.")
    return items.filter( it => {
      return it.hasOwnProperty(searchField) && (it[searchField] == expectedValue);
    });
  }
}
