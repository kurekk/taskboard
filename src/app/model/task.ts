export class Task {
  constructor(
    public id: number,
    public title: string,
    public description: string,
    public status: string,
    public assignee: string,
    public date: Date
  ) {}
}
