import {Component, OnInit} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {FormGroup, FormControl} from '@angular/forms';
import {Task} from '../model/task'

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  categories = [
    "TODO",
    "IN PROGRESS",
    "REVIEW",
    "DONE"
  ]

  /*
   * {
   *  boards: [
   *    {
   *      id: 0,
   *      name: "",
   *      password: "",
   *      tasks: [
   *        {
   *          id: 0,
   *          title: "",
   *          description: "",
   *          date: "",
   *          status: "",
   *          assignee : {
   *            id: "",
   *            name: "",
   *            surname: ""
   *          }
   *      ]
   *  ],
   * 
   */

  tasks = [];

  tasksSettings = {};

  view = {
    settings: {
      filters: {
        collapse: true
      }
    }
  }

  newTask = new Task(0, "", "", "", "", null);

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
  }

  onTaskStatusChange(newStatus, taskId) {
    let taskIndex = this.findTaskIndex(taskId);
    if (taskIndex < 0) {
      return;
    }
    this.tasks[taskIndex]["status"] = newStatus;
    this.tasks = this.tasks.slice();
  }

  changeTaskDisplayStatus(taskId) {
    if (!this.tasksSettings.hasOwnProperty(taskId)) {
      this.tasksSettings[taskId] = {"display": true};
    } else {
      let currentDisplayVal = this.tasksSettings[taskId]["display"];
      this.tasksSettings[taskId]["display"] = !currentDisplayVal;
    }
  }

  private showDescription(taskId): boolean {
    return this.tasksSettings.hasOwnProperty(taskId) &&
      this.tasksSettings[taskId]['display'];
  }

  private findTaskIndex(taskId) {
    for (let i = 0; i < this.tasks.length; i++) {
      if (this.tasks[i]["id"] == taskId) {
        return i;
      }
    }

    return -1;
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
        this.addNewTask(
          this.newTask.title, 
          this.newTask.description,
          "",
          "TODO"
        );
      this.newTask = new Task(0, "", "", "", "", null);
    }, (reason) => {
      console.log("reason");
    });
  }

  private addNewTask(title: string, description: string, assignee: string,
                      status: string) {
    let date = new Date().toISOString();
    let newTask = {
      id: Math.floor(Math.random() * 1000),
      title: title,
      assignee: assignee,
      description: description,
      date: date,
      status: status
    }

    this.tasks.push(newTask);
    this.tasks = this.tasks.slice();

    console.log("Added new task: ", newTask);
  }

}
